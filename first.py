import cv2
import logging
import math
import numpy as np
from util import load_mnist
from basic_math import matrix_mean, matrix_stddev

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def generate_mean_image(X, filename="mean_train.png"):
    logging.info("Generating {}".format(filename))
    X_mean = matrix_mean(X)
    return cv2.imwrite(filename, np.reshape(X_mean, (28, 28)))

def generate_stddev_image(X, filename="stddev_train.png"):
    logging.info("Generating {}".format(filename))
    X_dev = matrix_stddev(X)
    return cv2.imwrite(filename, np.reshape(X_dev, (28, 28)))

def main():
    X_train, y_train = load_mnist("train", "./data/")
    X_test, y_test = load_mnist("t10k", "./data/")
    X_train = np.reshape(X_train, (-1, 784))
    X_test = np.reshape(X_test, (-1, 784))

    uniq_nums = set(y_train)
    for num in uniq_nums:
        current_x_train = X_train[y_train == num]
        current_x_test = X_train[y_train == num]
        generate_mean_image(current_x_train, "out/{0}_mean.png".format(num))
        generate_stddev_image(current_x_train, "out/{0}_stddev.png".format(num))
    return


if __name__ == "__main__":
    main()
