from scipy.linalg import eigh
import numpy as np


def _eigvalsh_to_eps(spectrum, cond=None, rcond=None):
    if rcond is not None:
        cond = rcond
    if cond in [None, -1]:
        t = spectrum.dtype.char.lower()
        factor = {'f': 1E3, 'd': 1E6}
        cond = factor[t] * np.finfo(t).eps
    eps = cond * np.max(abs(spectrum))
    return eps


class MultivariateNormal:
    # Taken some inspiration from the scipy multivariate normal calculator
    # The traditional method of calculating the log pdf of a multivariate normal distro.
    # was causing Out Of Memory errors on large inputs, like the MNIST dataset images
    # Hence, I had to tweak my code to follow the scipy impl
    def __init__(self):
        return None

    def _infer_dim_mean_cov(self, mean, cov):
        dim = mean.size
        mean = np.asarray(mean, dtype=float)
        cov = np.asarray(cov, dtype=float)
        if cov.ndim == 1:
            cov = np.diag(cov)
        return dim, mean, cov

    def _process_vectors(self, x, dim):
        x = np.asarray(x, dtype=float)
        if x.ndim == 0:
            x = x[np.newaxis]
        elif x.ndim == 1:
            if dim == 1:
                x = x[:, np.newaxis]
            else:
                x = x[np.newaxis, :]
        return x

    def _decomp(self, x):
        eigenvalues, eigenvectors = eigh(x)
        threshold = _eigvalsh_to_eps(eigenvalues)
        dim = len(eigenvalues[eigenvalues > threshold])

        evalues_pseudo_inverse = np.array([0 if abs(x) <= threshold else 1 / x for x in eigenvalues], dtype=float)
        cov_pseudo_inverse = np.multiply(eigenvectors, np.sqrt(evalues_pseudo_inverse))
        return dim, cov_pseudo_inverse

    def logpdf(self, x, mean=None, cov=None):
        log_2_pi = np.log(2 * np.pi)
        dim, mean, cov = self._infer_dim_mean_cov(mean, cov)
        x = self._process_vectors(x, dim)
        sign, log_det_cov = np.linalg.slogdet(cov)
        x_minus_mean = x - mean
        dim, cov_pseudo_inverse = self._decomp(cov)
        mean_cov = np.sum(np.square(np.dot(x_minus_mean, cov_pseudo_inverse)), axis=-1)
        return -0.5 * (dim * log_2_pi + log_det_cov + mean_cov)

    def pdf(self, x, mean=None, cov=None):
        pdf = self.logpdf(x, mean, cov)
        return np.exp(pdf)

def pdf(x, mean, cov):
    """
    This method _will_ throw OOM exceptions, on large inputs like the MNIST
    dataset images. Works fine for smaller inputs, though.
    """
    dim, mean, cov = MultivariateNormal()._infer_dim_mean_cov(mean, cov)
    x = x[:, np.newaxis]
    det_cov = np.linalg.det(cov)
    inv_cov = np.linalg.inv(cov)
    one = 1 / np.sqrt(((2 * np.pi) ** dim) * det_cov)
    x_minus_mean = x - mean
    two_a = -0.5 * (np.transpose(x_minus_mean) * inv_cov * x_minus_mean)
    two = np.exp(two_a)
    three = one * two
    return np.diag(np.squeeze(three))
