import math
import numpy as np


def array_mean(X):
    return float(sum(X)) / len(X)


def matrix_mean(X):
    rows, cols = len(X), len(X[0])
    means = [0.0 for _ in range(cols)]
    for col in range(cols):
        means[col] = array_mean(X[:, col])
    return np.array(means)


def array_stddev(X):
    X_mean = array_mean(X)
    return math.sqrt(array_mean(abs(X - X_mean) ** 2))


def matrix_stddev(X):
    rows, cols = X.shape
    X_mean = matrix_mean(X)
    devs = [0.0 for _ in range(cols)]
    for col in range(cols):
        devs[col] = array_stddev(X[:, col])
    return np.array(devs)


def array_variance(X):
    return array_stddev(X) ** 2


def matrix_variance(X):
    return matrix_stddev(X) ** 2


def array_covariance(x1, x2):
    if len(x1) != len(x2):
        raise Exception("Length of arrays is not the same")
    l = len(x1)
    x1_mean = array_mean(x1)
    x2_mean = array_mean(x2)
    return sum((x1 - x1_mean) * (x2 - x2_mean)) / (l - 1)


def matrix_covariance(X):
    samples = len(X)
    return np.array([[array_covariance(X[idx], X[jdx]) for jdx in range(samples)] for idx in range(samples)])


def test():
    x = np.arange(0, 10)
    y = np.arange(5, 15)
    c = [x, y]
    assert (np.mean(x) == array_mean(x)), "Custom Mean formula is incorrect"
    assert (np.std(x) == array_stddev(x)), "Custom Std Dev formula is incorrect"
    assert (np.var(y) == array_variance(y)), "Custom Variance formula is incorrect"
    assert (np.cov(c, rowvar=True).all() == matrix_covariance(c).all()), "Custom Matrix Covariance formula is incorrect"
    return


if __name__ == "__main__":
    test()
