import logging
import numpy as np
from multivariate_normal import MultivariateNormal
from util import load_mnist
from basic_math import matrix_mean, matrix_variance

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)

class BayesClassifier:
    def fit(self, X, y, smoothing=0.001):
        self.means = {}
        self.variances = {}
        self.probability_of = {}
        self.unique_nums = set(y)

        for num in self.unique_nums:
            current_x = X[y == num]
            self.means[num] = matrix_mean(current_x)
            # Adding smoothing to avoid singular matrix
            self.variances[num] = matrix_variance(current_x) + smoothing
            self.probability_of[num] = float(len(y[y == num])) / len(y)
        return

    def predict(self, X):
        samples, features = X.shape
        num_uniq_labels = len(self.unique_nums)
        predictions = np.zeros((samples, num_uniq_labels))
        for num in self.unique_nums:
            mean = self.means[num]
            cov = self.variances[num]
            predictions[:, num] = MultivariateNormal().logpdf(X, mean=mean, cov=cov) + np.log(self.probability_of[num])
        return np.argmax(predictions, axis=1)

    def score(self, X, y):
        predictions = self.predict(X)
        correct_predictions = sum(predictions == y)
        return correct_predictions / len(y)

def main():
    X_train, y_train = load_mnist("train", "./data/")
    X_test, y_test = load_mnist("t10k", "./data/")
    X_train = np.reshape(X_train, (-1, 784))
    X_test = np.reshape(X_test, (-1, 784))

    model = BayesClassifier()
    logging.info("Training model")
    model.fit(X_train, y_train)
    logging.info("Done training model")

    accuracy = model.score(X_train, y_train)
    logging.info("Training set accuracy: {}".format(accuracy))
    accuracy = model.score(X_test, y_test)
    logging.info("Testing set accuracy: {}".format(accuracy))
    return

if __name__ == '__main__':
    main()

