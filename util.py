import numpy as np

def load_mnist(prefix, folder):
    int_type = np.dtype(np.int32).newbyteorder('>')
    n_meta_data_bytes = 4 * int_type.itemsize

    img_file_name = "{0}/{1}-images-idx3-ubyte".format(folder, prefix)
    data = np.fromfile(img_file_name, dtype='ubyte')
    magic_bytes, n_images, width, height = np.frombuffer(data[:n_meta_data_bytes].tobytes(), int_type)
    data = data[n_meta_data_bytes:].astype(dtype=np.float32).reshape([n_images, width, height])

    labels_file_name = "{0}/{1}-labels-idx1-ubyte".format(folder, prefix)
    labels = np.fromfile(labels_file_name, dtype='ubyte')[2 * int_type.itemsize:]

    return data, labels
